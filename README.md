## 简要说明  
1、这是一个工具脚本，它可以将innobackup批量备份的数据恢复到目标数据库中，侵入性小、简便了手动恢复的过程；  
2、它的恢复原理来自于innobackup官方文档；  
3、适用于mysql8以下版本，表引擎为innodb；  
4、每一个版本都是更新迭代的，请始终使用最新版本；  

## 版本说明
v1版本为原始版本，存在bug（会忽略第一个表的数据恢复）  
v2版本为新版本，通过指定脚本参数和配置文件运行；  
v1和v2恢复数据的来源都基于使用innobackup批量表备份，以下是批量备份表的命令：
```commandline
innobackupex --defaults-file=/etc/my.cnf --user=root --password=123456 --tables-file=target_tableName.txt --no-timestamp ./
```

target_tableName.txt 内容格式：
```text
hpdb.user
hpdb.system
fcode.user
newcode.system
hpdb.testetinfo
```

## v1版本使用说明
——准备好mysql实例  
略...  
——导入数据库表结构  
略...  
——修改脚本内的参数
```commandline
1、修改mysql连接参数，在54行
修改 mysql_cmd(mysql_name, sql_cmd) 方法内 host、user、port、password参数；

2、修改路径参数，在138行左右
mysql_datadir = "/usr/local/mysql/tableBatch_back_20220424-113801"  # mysql的数据目录(不需要携带/)
txt_path = "target_backupTable.txt"  	# 定义txt文件路径
data_path = "./data/"  	# 原数据目录的路径(携带/)
docker_userGroup = "polkitd:mysql"    # 指定的用户和用户组权限(使用docker恢复无需修改)
```
——python3 运行脚本
```commandline
python3 import_tableSpace_v1.0.py
```

## v2版本使用说明
### 使用示例
1、准备好innobackup批量备份表的数据  
如：tableBatch_back_20230513-010002  
包含 target_backupTable.txt 文件  
2、准备mysql示例和空表环境  
3、配置好config.conf内的参数  
4、运行
```shell
python3 import_tableSpace_v2.0.py --config config.json --txt target_backupTable.txt --import-dataDir tableBatch_back_20230513-010002
```
### v2.0版本新增功能

1、脚本参数
```commandline
[root@mq3 shell_dev]# python3 import_tableSpace_v2.0.py -h 
usage: import_tableSpace_v2.0.py [-h] --config CONFIG --txt TXT --import-dataDir IMPORTDATADIR

optional arguments:
  -h, --help            show this help message and exit
  --config CONFIG       记录mysql相关参数的json文件
  --txt TXT             记录 db.table 的txt文件
  --import-dataDir IMPORTDATADIR
                        要导入的原始备份数据文件目录(innobackup批量备份的数据目录)
```

2、脚本运行前置环境检查；  
检查前置运行条件，满足条件后会通过让用户输入确认的方式来进行后续的恢复操作；
```commandline
[root@mq3 shell_dev]# python3 import_tableSpace_v2.0.py --config config.json --txt target_backupTable.txt --import-dataDir tableBatch_back_20230513-010002
2023.05.17-15:43:11    前置运行环境检查结果: 正常
确认以下操作后即可开始执行
---------
1.备份数据回滚检测:
  [shell]# innobackupex --apply-log --export tableBatch_back_20230513-010002/
2.临时关闭外键约束检查[全局]:
  [mysql]> SET @@global.Foreign_key_checks=0;
  [mysql]> SELECT @@global.Foreign_key_checks;
---------
是否开始迁移数据(y/n):
```
3、config配置文件

config配置文件中主要为mysql的一些关键参数
```json
{
    "Mysql_ip":"192.168.2.15",
    "Mysql_port":"3306",
    "Mysql_user":"root",
    "Mysql_passwd":"123456",
    "Mysql_datadir":"/usr/local/mysql/data/",
    "userGroup":"mysql:mysql"
}
```
其中：  
Mysql_datadir：mysql数据存放的目录；  
userGroup：mysql数据存放目录的用户和用户组；

### v2.1版本改动
1.为了降低与py版本耦合度,执行shell的方法不再依赖subprocess.run(shell),而使用os.system(shell)  
2.参数 --import-dataDir 改为 --import  
>使用方法和V2.0相同