
## innobackup备份常用参数
```commandline
--defaults-file=/etc/my.cnf         # [必须作为第一个参数]
--user=root
--password=123456
--host=192.168.1.121
--socket=/var/run/mysql/mysql.sock  # [可选]
--tables-file=/home/target_tableName.txt    # [可选]
```

## innobackup备份指定整库:
(多个库用空格分割)
```shell
innobackupex --defaults-file=/etc/my.cnf --user=root --password=123456 --databases="hpdb" ./
```
## innobackup通过txt备份指定表:
(--tables-file指定txt的绝对路径)
```shell
innobackupex --defaults-file=/etc/my.cnf --user=root --password=123456 --tables-file=target_tableName.txt ./
```
## 事务回滚检测命令:
```shell
innobackupex --apply-log --export ./{innobackup_backup_file}
```
## config.json 格式:
```json
{
    "Mysql_ip":"192.168.2.15",
    "Mysql_port":"3306",
    "Mysql_user":"root",
    "Mysql_passwd":"123456",
    "Mysql_datadir":"/usr/local/mysql/data/",
    "userGroup":"mysql:mysql"
}
```